import java.util.*;

public class Test{

    private static String repeat1(char c, int i){
        String ans = "";
        for(int j =0; j<i; j++){
            ans+=c;
        }
        return ans;
    }   

    private static String repeat2(char c, int i){
        StringBuilder ans = new StringBuilder();
        for(int j =0; j<i; j++){
            ans.append(c);
        }
        return ans.toString();
    }

    public static void main(String args[]){
        long startTimeNs = System.currentTimeMillis();
        repeat1('*',100000);
        long timeNs = System.currentTimeMillis() - startTimeNs;
        System.out.println("finished repeat1 in "+ timeNs+" milliseconds");
        long startTimeNs1 = System.currentTimeMillis();
        repeat2('*',100000);
        long timeNs1 = System.currentTimeMillis() - startTimeNs1;
        System.out.println("finished repeat2 in " + timeNs1 +" milliseconds");
    }
}